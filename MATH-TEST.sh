#! /bin/bash

clear
if [ -z $1 ]
# For the sake of speed, the first three menues can be bypassed by using arguments.
then
	while [ -z $selection ]; do
			clear
			echo -e " Select what you want to be tested on ! \n Addition (1) \n Subtraction (2) \n Multiplication (3) \n Division (4)" 
			read selection
	done
else
selection=$1
fi

clear




if [ -z $2 ]
# For the sake of speed, the first three menues can be bypassed by using arguments.
then
	while [ -z $s2 ]; do
	clear
	echo "Easy(1), Hard(2), or Impossible(3)?"
	read s2
	done
else
s2=$2
fi

clear

if [ -z $3 ]
# For the sake of speed, the first three menues can be bypassed by using arguments.
then
	while [ -z $name ]; do
			clear
			echo "What is your name?"
			read name
		done
else
name=$3
fi

case $s2 in

# f,j,k,m,n,o determine the eqeuations. 
# The following equation determines the first number in each question.
# seq f j k
# The following determines the second.
# seq m n o
# x and z determine the score, with x determining the score added, and z determing the score subtracted. The weight is further manipulated depending on the type of operators used.
	1) 
	f=5; j=2; k=11
	m=7; n=-2; o=3
	x=1; z=2	
	echo $s2
		;;
	2)
	f=31; j=8; k=55
	m=19; n=-6; o=7
	x=3; z=2
	
		;;
	3)	
	f=17; j=16; k=65
	m=25; n=-8; o=9
	x=4; z=1
		;;
	*) 
		clear
		echo -e "ERROR! Invalid input!\n[1-4] [1-3] [Any string]\n $selection $s2 $name"
		exit 69;;

esac
## The following commented variables are overrides for the purpose of troubleshooting.
# Seqeuence for number first number
#f=1
#j=1
#k=2
# Sequency for second number
#m=1	
#n=1
#o=2
# Score weight
#x=1
#z=1
# 
clear
score=0
case $selection in

	1) 
		## Equation asker	
		for a in `seq $f $j $k`
			do
				for b in `seq $m $n $o`
					do
						c=`expr $a + $b`
						echo "What is $a + $b?"
						read d
						clear
						if [ $d -eq $c ]; then
							echo "Correct!"
							score=`expr $score + $x`
							echo "Your current score is $score"
						else
							clear
							score=`expr $score - $z`
							echo -e "INCORRECT!! \n Your current score is $score"
						fi
					done
			done
	;;
	2)

		## Equation asker	
		x=`expr 2 \* $x`
		for a in `seq $f $j $k`
			do
				for b in `seq $m $n $o`
					do
						c=`expr $a - $b`
						echo "What is $a - $b"
						read d
						clear
						if [ $d -eq $c ]; then
							echo "Correct!"
							score=`expr $score + $x`
							echo "Your current score is $score"
						else
							clear
							score=`expr $score - $z`
							echo -e "INCORRECT!! \n Your current score is $score"
						fi
					done
			done
		;;
	3)

		x=`expr 3 \* $x`
		## Equation asker	
		for a in `seq $f $j $k`
			do
				for b in `seq $m $n $o`
					do
						c=`expr $a \* $b`
						echo "What is $a * $b"
						read d
						clear
						if [ $d -eq $c ]; then
							echo "Correct!"
							score=`expr $score + $x`
							echo "Your current score is $score"
						else
							clear
							score=`expr $score - $z`
							echo -e "INCORRECT!! \n Your current score is $score"
						fi
					done
			done
		;;
	4) azfunction="Division"

		x=`expr 8 \* $x`
		y=`expr $y / 2`
		## Equation asker	
		for a in `seq $f $j $k`
			do
				for b in `seq $m $n $o`
					do
						c=`bc <<< "scale=1;$a / $b"`

						echo "What is $a / $b"
						echo -e "Remember:\n TRUNCATE (NOT rount) to 1 decimal place, ALWAYS include a decimal place, and don't include a leading zero if your answer is greator than -1 and less than 1.(ie. write 1/2 as .5)"
						read d
						clear
						if [ $d == $c ]; then
							echo "Correct!"
							score=`expr $score + $x`
							echo "Your current score is $score"
						else
							clear
							score=`expr $score - $z`
							echo -e "INCORRECT!! \n Your current score is $score"
						fi
					done
			done
		;;
	*) 
		clear
		echo -e "ERROR! Invalid input!\n[1-4] [1-3] [Any string]\n $selection	$s2	$name"



esac
clear
echo "Your final score is $score"
# Saves and shows scores.
echo "$score	$name	`date --rfc-3339'=seconds'`" >> scores

echo "[STANDING] [SCORE] [PLAYER] [TIME]"
sort -g -r scores | nl -s	":	" | head -5
echo "ENTER to exit."
read nothing

