I've always wanted to be a programmer and an open source "contributor," and I've always wanted to have a project on Gitlab. And when I first heard of *Good Luck With That Public Liscence (GLWTPL),* I found the idea so hilarious that I wanted to publish my first "program" under it. So, here is my "contribution" to society:

# MATH-QUIZ

MATH-QUIZ is a game that tests the user on their arithmatic skills. It is written in Bash (lol). 

This was part of a school project that involved scripting. As per the rubric, it contains at least one `if` and `case` statement, and at least one `for` and `while` loop.

## Depends On

- `bash`
- `bc`
